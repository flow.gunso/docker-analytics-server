import shelve

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from .settings import get_settings


settings = get_settings()
api = FastAPI()
api.add_middleware(
    CORSMiddleware,
    allow_credentials=True,
    allow_origins=["*"],
    allow_methods=["*"],
    allow_headers=["*"]
)

@api.get("/")
async def root():
    with shelve.open(str(settings.shelf)) as shelf:
        pulls = shelf["pulls"]
        stars = shelf["stars"]
        pulls_stars = shelf["pulls_stars"]

    return {
        "pulls": pulls,
        "stars": stars,
        "pulls_stars": pulls_stars
    }

