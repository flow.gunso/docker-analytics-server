from datetime import datetime
import shelve

from .settings import get_settings, database_schema


settings = get_settings()

def init():
    with shelve.open(str(settings.shelf), flag="c") as shelf:
        for key, value in database_schema.items():
            try:
                shelf[key]
            except KeyError:
                shelf[key] = value

def write_to_shelf(function):
    def decorator(*args, **kwargs):
        with shelve.open(str(settings.shelf), flag="w") as shelf:
            return function(*args, **kwargs, shelf=shelf)
    return decorator

@write_to_shelf
def store_pull_star_count(date: datetime, pulls: int, stars: int, shelf: shelve.DbfilenameShelf):
    pulls_stars = shelf["pulls_stars"]
    pulls_stars.append({"date": date, "pulls": pulls, "stars": stars})
    shelf["pulls_stars"] = pulls_stars

@write_to_shelf
def store_star_count(date: datetime, count: int, shelf: shelve.DbfilenameShelf):
    stars = shelf["stars"]
    stars.append({date.isoformat(): count})
    shelf["stars"] = stars

@write_to_shelf
def store_pull_count(date: datetime, count: int, shelf: shelve.DbfilenameShelf):
    pulls = shelf["pulls"]
    pulls.append({date.isoformat(): count})
    shelf["pulls"] = pulls

@write_to_shelf
def store_payload(date: datetime, payload: dict, shelf: shelve.DbfilenameShelf):
    payloads = shelf["payloads"]
    payloads.append({date.isoformat(): payload})
    shelf["payloads"] = payloads

@write_to_shelf
def store_index(date: datetime, shelf: shelve.DbfilenameShelf):
    index = shelf["index"]
    index.append(date.isoformat())
    shelf["index"] = index
