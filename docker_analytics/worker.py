from datetime import datetime
import shelve
import json

from celery import Celery
from celery.schedules import crontab
import requests

from .settings import get_settings
from .storage import store_index, store_payload, store_pull_star_count


settings = get_settings()
worker = Celery(broker=settings.broker)

@worker.on_after_configure.connect
def setup_worker(sender, **kwargs):
    sender.add_periodic_task(
        crontab(
            minute=settings.minute,
            hour=settings.hour,
            day_of_month=settings.day_of_month,
            month_of_year=settings.month_of_year,
            day_of_week=settings.day_of_week),
        fetch_docker_registry_data.signature(),
    )

@worker.task()
def fetch_docker_registry_data():
    r = requests.get(settings.endpoint)

    if r.status_code == 200:
        now = datetime.now()
        payload = r.json()
        store_index(now)
        store_payload(now, payload)
        store_pull_star_count(now, payload["pull_count"], payload["star_count"])

    elif r.status_code == 429:
        pass  # Delay the task to after the rate limit reset.
