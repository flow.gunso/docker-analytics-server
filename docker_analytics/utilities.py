import os


def get_environment_variable(name, default=None):
    """Helper function for Django's settings to get an environment variable.

    Either the environment variable allows a default value or it does not. When no
    default value is provided and the environment variable was not found an
    ImproperlyConfigured exception is raised.

    Changelog:
    1.1 | 2021/02/25
      Modified:
      - Switch to f-strings
      - Use line length of 88
    1.0 | 2020/02/13
      - Initial release 
    """

    try:
        variable = os.environ[name]
    except KeyError:
        if default is None:
            raise SystemExit(
                f"Environment variable {name} was not found but is required."
            )
        return default
    return variable
