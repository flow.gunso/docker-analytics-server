from os import environ
from pathlib import Path

from pydantic import BaseSettings

from .utilities import get_environment_variable


database_schema = {
    "index": [],
    "payloads": [],
    "pulls_stars": [],
    "pulls": [],
    "stars": [],
}

def get_settings():
    environment_target = get_environment_variable("ENVIRONMENT_TARGET")
    if environment_target == "production":
        return ProductionSettings()
    elif environment_target == "development":
        return DevelopmentSettings()
    else:
        raise SystemExit(f"Unknown environment target {environment_target}")


class GlobalSettings(BaseSettings):
    minute: str = get_environment_variable("MINUTE", "22")
    hour: str = get_environment_variable("HOUR", "*")
    day_of_month: str = get_environment_variable("DAY_OF_MONTH", "*")
    month_of_year: str = get_environment_variable("MONTH", "*")
    day_of_week: str = get_environment_variable("DAY_OF_WEEK", "*")
    repository: str = get_environment_variable("REPOSITORY")
    endpoint: str = f"https://hub.docker.com/v2/repositories/{repository}/"


class DevelopmentSettings(GlobalSettings):
    shelf: Path = Path(__file__).parent.parent.joinpath(".env", "shelf")
    broker: str = get_environment_variable("BROKER", "amqp://localhost:5672//")


class ProductionSettings(GlobalSettings):
    shelf: Path = Path("/data/shelf")
    broker: str = get_environment_variable("BROKER", "amqp://guest@rabbitmq:5672//")
