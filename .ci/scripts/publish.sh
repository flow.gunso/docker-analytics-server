#!/bin/bash

SCRIPT=$(basename ${BASH_SOURCE[0]})

usage() {
    echo "Build and push the Docker images to Gitlab's registry."
    echo ""
    echo "$SCRIPT [options] [build_target]"
    echo
    echo "Available build targets are:"
    echo "  api             build the API"
    echo "  worker          build the worker"
    echo "By default, not specifying a build target, all targets will be built."
    echo "On the other hand, you can only explicitly build a single target."
    echo
    echo "Options are:"
    echo "  -h, --help      Print this helper."

    if [ "$1" ]; then
        echo ""
        echo "Exited with error: $1"
        exit 1
    else
        exit 0
    fi
}
 
while (( "$#" )); do
    case "$1" in
        -h|--help)
            usage
            ;;
        -*|--*)
            usage "Unknown option $1"
            ;;
        *) # Break to reduce footprint.
            break
            ;;
    esac
done

# Reject if more than one argument provided.
if [ $# -gt 1 ]; then
    usage "Too many build target given."
# Set target to both the api and the worker is no argument provided.
elif [ $# -eq 0 ]; then
    targets=("api" "worker")
# Set the target to a valid one according to the provided argument. 
else
    case "$1" in
        api)
            targets=("api")
            ;;
        worker)
            targets=("worker")
            ;;
        *)
            usage "Unknown build target $1"
            ;;
    esac
fi

# Validate the tag against the SemVer.
semver_regex="^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$"
semver_match=$(echo $CI_COMMIT_TAG | grep -P $semver_regex)
if [ -z $semver_match ]; then
    echo "Version tag $CI_COMMIT_TAG does not adhere to SemVer!"
    exit 1
fi

# Capture version components and assign the tag list.
declare -A targets_tags
major=$(echo $CI_COMMIT_TAG | perl -pe "s/$semver_regex/\1/")
minor=$(echo $CI_COMMIT_TAG | perl -pe "s/$semver_regex/\2/")
revision=$(echo $CI_COMMIT_TAG | perl -pe "s/$semver_regex/\3/")
for target in "${targets[@]}"; do
    tags="$target "
    tags+="$target-$major "
    tags+="$target-$major.$minor "
    tags+="$target-$major.$minor.$revision"
    targets_tags[$target]=$tags
done

docker login --username $CI_REGISTRY_DEPLOY_USERNAME --password $CI_REGISTRY_DEPLOY_PASSWORD $CI_REGISTRY
for target in "${!targets_tags[@]}"; do
    docker build --file .ci/files/Dockerfile --build-arg BUILD_TARGET=$target --tag $CI_PROJECT_NAME:$target-build .
    for tag in ${targets_tags[$target]}; do
        docker tag $CI_PROJECT_NAME:$target-build $DOCKER_ANALYTICS_REGISTRY_IMAGE:$tag
        docker push $DOCKER_ANALYTICS_REGISTRY_IMAGE:$tag
    done
done
