#!/bin/sh

python -c "from docker_analytics.storage import init; init()"
celery --app docker_analytics.worker worker --beat
