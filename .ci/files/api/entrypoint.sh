#!/bin/sh

python -c "from docker_analytics.storage import init; init()"
uvicorn --host 0.0.0.0 docker_analytics.api:api
