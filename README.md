**Docker Analytics Server**

Server side of [Docker Analytics](https://gitlab.com/flow.gunso/docker-analytics).

# Dependencies
* FastAPI as the HTTP framework
* Celery as the periodic worker fetching data from Docker
* Standard Python's shelve to store data

# Licence
MIT
